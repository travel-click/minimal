import React from 'react'
import { Row, Col, Spin, Carousel, Button, List } from 'antd'
import { TravelCloudClient, formatCurrency, Cart, validateLegacyHotelParams } from 'travelcloud-antd'
import { LegacyHotelsDetailForm } from 'travelcloud-antd/components/legacy-hotel-detail-form'
import { GoogleMapReact } from 'google-map-react'
import { NoResult } from 'travelcloud-antd/components/no-result'
import config from '../customize/config'
import Head from 'next/head'
import Router from 'next/router'
import Link from 'next/link'

export default class extends React.PureComponent<{ cart: Cart, query: { [key: string]: string } }> {
  private client = new TravelCloudClient(config.tcUser);

  static async getInitialProps(context) {
    const query = context.query
    return { query }
  }

  state = {
    hotel: {} as any,
    loading: true,
    hotelId: this.props.query.hotelId,
    checkInDate: this.props.query.checkInDate,
    checkOutDate: this.props.query.checkOutDate,
    adults: this.props.query.adults || 2,
    children: this.props.query.children || 0,
    address: '' as string,

    // selected hotel state properties
    selectedHotel: {
      hotels: {} as any,
      loading: false,
      cityCode: this.props.query.cityCode,
      checkInDate: this.props.query.checkInDate,
      checkOutDate: this.props.query.checkOutDate,
      adults: this.props.query.adults || 2,
      children: 0
    }

  }

  async componentDidMount() {
    // update state with loading true
    this.setState({ loading: true })

    // search hotel with default state
    const hotel = await this.client.legacyHotel(this.state)
    const address = `${hotel.result.contact.address.streetName},
                    ${hotel.result.contact.address.city},
                    ${hotel.result.contact.address.postalCode}`

    // update state with hotel
    this.setState({ loading: false, hotel, address })
  }

  async paramsChange(value) {

    // Params to be passed to retrive hotel availability
    const paramsRefresh = {
      adults: "1",
      checkInDate: value.checkInDate,
      checkOutDate: value.checkOutDate,
      children: "0",
      cityCode: value.cityCode,
      hotelId: this.state.hotel.result.hotelId

    }
    // update state with filter values and loading true
    this.setState({ loading: true, ...value })

    // search hotel with filter values
    const hotel = await this.client.legacyHotel(paramsRefresh)

    // update state with hotel and loading false
    this.setState({ loading: false, hotel })
  }

  /**
  * Describes the elements on the Hotel page
  */
  render() {
    const params = validateLegacyHotelParams(this.props.query)
    /**
     * Google marker pin
     */
    if (params == null) return <div>
      Invalid hotel params
    </div>
    return (
      <div>
        <Head>
          <title>{config.defaultTitle} | Hotel</title>
        </Head>
        {this.state.loading
          ? <div style={{ minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32 }}>
            <NoResult
              style={{ paddingTop: 128 }}
              response={this.state.hotel}
              loading={this.state.loading}
              type="hotel">
              Cannot load selected hotel. Please&nbsp;
                <Link href={{
                pathname: '/hotel', query: {
                  cityCode: this.props.query.cityCode,
                  checkInDate: this.props.query.checkInDate,
                  checkOutDate: this.props.query.checkOutDate,
                  adults: this.props.query.adults || 2,
                  children: this.props.query.children || 0
                }
              }}>
                <a>select a different hotel</a>
              </Link>
              &nbsp;or different dates.
              </NoResult>
          </div>
          : <div style={{ maxWidth: 1400, margin: '32px auto' }}>
            <Row>
              <h1 style={{ fontSize: 40, fontWeight: 'normal', marginBottom: 0 }}>{this.state.hotel.result.name}</h1>
            </Row>
            <Row style={{ backgroundColor: '#fff', margin: '32px 0' }}>
              <Col span={8}>
                <Carousel autoplay>
                  {this.state.hotel.result.images.map((photo, index) =>
                    <div key={index}><div style={{
                      backgroundImage: `url(${photo.image})`,
                      height: '400px',
                      width: '100%',
                      backgroundSize: 'cover',
                      backgroundPosition: 'center'
                    }} /></div>)}
                </Carousel>
              </Col>
              <Col span={16}>
                <table>
                  <tbody>
                    {this.state.hotel.result.rooms.map((room, index) =>
                      room.prices.map((price, index2) =>
                        <tr key={index+'-'+index2}>
                          {index2 === 0 && <td rowSpan={room.prices.length}>
                            {room.description}
                          </td>}
                          <td>
                            {price.meal}
                          </td>
                          <td>
                            {price.occupancy.adults} adults
                                {parseInt(price.occupancy.children) > 0
                              ? price.occupancy.children + 'children'
                              : ''}
                          </td>
                          <td>
                            ${price.price}
                          </td>
                          <td>
                            <Button onClick={() => {
                              this.props.cart.reset().addLegacyHotel(
                                this.state.hotel.result, {
                                  room_id: room.id,
                                  price_code: price.code,
                                  params
                                })
                              Router.push('/checkout')
                            }}>
                              Add to cart
                                </Button>
                          </td>
                        </tr>
                      )
                    )}
                  </tbody>
                </table>
              </Col>
            </Row>
            <div>
              <h2>Description</h2>
              <div>{this.state.hotel.result.description}</div>
              <h2>Facilities</h2>
              <div className="hotel-features">
                {this.state.hotel.result.facilities.map(facility => facility.fee === 'Y' ? <div className="hotel-facility">{facility.text + '*'}</div> : <div className="hotel-facility">{facility.text}</div>)}
              </div>
              <div>*Additional fees may apply</div>
              <LegacyHotelsDetailForm
                client={this.client}
                onChange={(value) => this.paramsChange(value)}
                value={this.state.selectedHotel}
                defaultCityCodes={['SIN', 'BKK', 'KUL', 'HKG']}
              />
              <h2>Location</h2>
              <h4>{this.state.address}</h4>
            </div>
          </div>}
      </div>
    )
  }
}