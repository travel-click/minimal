import React from 'react'
import { Button } from 'antd'
import { TravelCloudClient, validateLegacyHotelsParams, validateFlightsParams } from 'travelcloud-antd'
import { LegacyHotelsParamsForm } from 'travelcloud-antd/components/legacy-hotels-params-form'
import { FlightsParamsForm } from 'travelcloud-antd/components/flights-params-form'
import config from '../customize/config'
import Router from 'next/router'


export default class extends React.PureComponent<any> {
  private client = new TravelCloudClient(config.tcUser)

  static async getInitialProps (context) {
    const client = new TravelCloudClient(config.tcUser)
    const query = context.query
    const document = await client.document({id: 1})
    return { query, document }
  }

  state = {
    hotelsParams: {
      cityCode: undefined,
      checkInDate: undefined,
      checkOutDate: undefined,
      adults: 2,
      children: 0
    },
    hotelsParamsInvalid: true,

    flightsParams: {
      source: 'tp',
      'od1.origin_airport.code': undefined,
      'od1.origin_datetime': undefined,
      'od2.origin_airport.code': undefined,
      'od2.origin_datetime': undefined,
      cabin: 'Y',
      ptc_adt: 1,
      ptc_cnn: 0,
      ptc_inf: 0,
    },
    flightsParamsInvalid: true,
  }

  render() {
    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <div dangerouslySetInnerHTML={{__html: this.props.document.result.content}} />
        <h2>Hotel search</h2>
        <LegacyHotelsParamsForm
          client={this.client}
          onChange={(value) => this.setState({hotelsParams: value, hotelsParamsInvalid: validateLegacyHotelsParams(value) == null})}
          value={this.state.hotelsParams}
          defaultCityCodes={['SIN', 'BKK', 'KUL', 'HKG']}>
          <LegacyHotelsParamsForm.CityCodeSelect style={{width: 200}} placeholder="Destination" />
          <LegacyHotelsParamsForm.CheckInDatePicker style={{width: 200}} placeholder="Check-in" />
          <LegacyHotelsParamsForm.CheckOutDatePicker style={{width: 200}}  placeholder="Check-out" />
          <LegacyHotelsParamsForm.AdultOccupancySelect style={{width: 200}} />
        </LegacyHotelsParamsForm>
        <Button
          type="primary"
          disabled={this.state.hotelsParamsInvalid}
          onClick={() => Router.push({
            pathname: '/hotels',
            query: this.state.hotelsParams
          })}>
          Search
        </Button>

        <h2>Flight search</h2>
        <FlightsParamsForm
          client={this.client}
          onChange={(value) => this.setState({flightsParams: value, flightsParamsInvalid: validateFlightsParams(value) == null})}
          value={this.state.flightsParams}
          defaultIataCodes={['SIN', 'BKK', 'KUL', 'HKG']}>
            <FlightsParamsForm.Od1OriginCodeSelect style={{width: '100%'}} />
            <FlightsParamsForm.Od2OriginCodeSelect style={{width: '100%'}} />
            <FlightsParamsForm.Od1OriginDepartureDatePicker style={{width: '100%'}} />
            <FlightsParamsForm.Od2OriginDepartureDatePicker style={{width: '100%'}} />
            <FlightsParamsForm.PtcAdtSelect />
            <FlightsParamsForm.PtcCnnSelect />
            <FlightsParamsForm.PtcInfSelect />
        </FlightsParamsForm>
        <Button
          type="primary"
          disabled={this.state.flightsParamsInvalid}
          onClick={() => Router.push({
            pathname: '/flights',
            query: this.state.flightsParams
          })}>
          Search
        </Button>
      </div>
    )
  }
}