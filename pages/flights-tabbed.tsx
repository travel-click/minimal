import React from 'react'
import { TravelCloudClient, Cart, validateFlightsParams, TcResponse, FlightsParams } from 'travelcloud-antd'
import { FlightsParamsAdvancedForm } from 'travelcloud-antd/components/flights-params-form'
import { FlightsResult } from 'travelcloud-antd/components/flights-result'
import { NoResult } from 'travelcloud-antd/components/no-result'
import config from 'travelcloud-antd/customize/config'
import Head from 'next/head'
import Router from 'next/router'
import { FlightSearch, FlightDetail } from 'travelcloud-antd/types'
import { Tabs } from 'antd';

const nullOrEmpty = (x) => x == null || x === ""

type TabData = {
  originalFlightsParams: FlightsParams,
  flightsParams: FlightsParams,
  flights: TcResponse<FlightSearch>,
  flightMap: {[key: string]: TcResponse<FlightDetail>},
  selection: {flight, fareId},
  linkedTab: number // for one way component seach
}

export default class extends React.PureComponent<{cart: Cart, query, openCart}> {
  private client = new TravelCloudClient(config.tcUser);

  static async getInitialProps (context) {
    const query = context.query
    return { query }
  }

  state = {
    tabIndex: -1,
    tabDatas: [] as TabData[],
    flightsParam: {
      source: 'travelport',
      'od1.origin_airport.code': this.props.query['od1.origin_airport.code'],
      'od1.origin_datetime': this.props.query['od1.origin_datetime'],
      'od1.destination_airport.code': this.props.query['od1.destination_airport.code'],
      'od2.origin_airport.code': this.props.query['od2.origin_airport.code'],
      'od2.origin_datetime': this.props.query['od2.origin_datetime'],
      'od2.destination_airport.code': this.props.query['od2.destination_airport.code'],
      ptc_adt: this.props.query.ptc_adt || 1,
      ptc_cnn: this.props.query.ptc_cnn || 0,
      ptc_inf: this.props.query.ptc_inf || 0,
      cabin: this.props.query.cabin || 'Y',
    },

    type: 'return',
    loadingFlights: false
  }

  swap() {
    const od1Origin = this.state.flightsParam["od1.origin_airport.code"]
    const od2Origin = this.state.flightsParam["od2.origin_airport.code"]
    const newValue = Object.assign({}, this.state.flightsParam)
    newValue["od1.origin_airport.code"] = od2Origin
    newValue["od2.origin_airport.code"] = od1Origin
    this.setState({flightsParam: newValue})
  }

  async componentDidMount() {
    if (!nullOrEmpty(this.state.flightsParam["od1.destination_airport.code"]) || !nullOrEmpty(this.state.flightsParam["od2.destination_airport.code"])) {
      this.typeChange('multi')
    }

    // check if current params are valid
    const validated = validateFlightsParams(this.state.flightsParam)

    if (validated != null) {
      this.addNewTabs(validated)
    }
  }

  async paramsChange(value) {
    this.setState({flightsParam: value})
  }

  typeChange = (type: string) => {
    let result
    if (type === 'return') {
      this.setState(Object.assign(this.state.flightsParam, {
        "od1.destination_airport.code": undefined,
        "od2.destination_airport.code": undefined
      }))
    }

    if (type === 'one') {
      this.setState(Object.assign(this.state.flightsParam, {
        "od1.destination_airport.code": undefined,
        "od2.destination_airport.code": undefined,
        "od2.origin_datetime": undefined
      }))
    }

    this.setState({type})
  }

  async addNewTabs(originalFlightsParams: FlightsParams) {
    originalFlightsParams = validateFlightsParams(originalFlightsParams)
    if (originalFlightsParams == null) return
    const oneWayComponentAirports = ['SYD']
    const oneWayComponentMode = originalFlightsParams['od2.origin_datetime'] != null
      && originalFlightsParams['od2.origin_datetime'].trim() != ''
      && (oneWayComponentAirports.indexOf(originalFlightsParams['od1.origin_airport.code']) !== -1
        || oneWayComponentAirports.indexOf(originalFlightsParams['od2.origin_airport.code']) !== -1
        || oneWayComponentAirports.indexOf(originalFlightsParams['od1.destination_airport.code']) !== -1
        || oneWayComponentAirports.indexOf(originalFlightsParams['od2.destination_airport.code']) !== -1)

    var tabDatas
    if (oneWayComponentMode) {
      const od1Params = {
        source: originalFlightsParams['source'],
        'od1.origin_airport.code': originalFlightsParams['od1.origin_airport.code'],
        'od1.origin_datetime': originalFlightsParams['od1.origin_datetime'],
        'od1.destination_airport.code': originalFlightsParams['od1.destination_airport.code'],
        'od2.origin_airport.code': originalFlightsParams['od2.origin_airport.code'],
        ptc_adt: originalFlightsParams['ptc_adt'],
        ptc_cnn: originalFlightsParams['ptc_cnn'],
        ptc_inf: originalFlightsParams['ptc_inf'],
        cabin: originalFlightsParams['cabin'],
      }

      const od2Params = {
        source: originalFlightsParams['source'],
        'od1.origin_airport.code': originalFlightsParams['od2.origin_airport.code'],
        'od1.origin_datetime': originalFlightsParams['od2.origin_datetime'],
        'od1.destination_airport.code': originalFlightsParams['od2.destination_airport.code'],
        'od2.origin_airport.code': originalFlightsParams['od1.origin_airport.code'],
        ptc_adt: originalFlightsParams['ptc_adt'],
        ptc_cnn: originalFlightsParams['ptc_cnn'],
        ptc_inf: originalFlightsParams['ptc_inf'],
        cabin: originalFlightsParams['cabin'],
      }

      // tabs are pushed to the end of the array
      // this is to ensure that tab indexes do not change
      // as a result, the ui needs to show the tabs in reverse order
      tabDatas = this.state.tabDatas.concat([
        {
          originalFlightsParams,
          flightsParams: od2Params,
          flights: {},
          flightMap: {},
          selection: null,
          linkedTab: this.state.tabDatas.length + 1
        },
        {
          originalFlightsParams,
          flightsParams: od1Params,
          flights: {},
          flightMap: {},
          selection: null,
          linkedTab: this.state.tabDatas.length
        }
      ])

    } else {
      tabDatas = this.state.tabDatas.concat([
        {
          originalFlightsParams,
          flightsParams: originalFlightsParams,
          flights: {},
          flightMap: {},
          selection: null,
          linkedTab: null
        }])
    }
    this.switchTab(tabDatas.length - 1, tabDatas)

  }

  async switchTab(tabIndex: number, tabDatas?: TabData[]) {
    if (tabDatas == null) tabDatas = this.state.tabDatas.slice()
    const flightsParams = tabDatas[tabIndex].flightsParams

    Router.push({
      pathname: Router.pathname,
      query: tabDatas[tabIndex].originalFlightsParams
    })

    if (tabDatas[tabIndex].flights.result != null) {
      return this.setState({
        tabIndex
      })
    }

    tabDatas[tabIndex].flights = {loading: true}
    this.setState({
      tabDatas, tabIndex, loadingFlights: true
    })

    const flights = await this.client.flights(flightsParams)

    // assume that the setState() before await has completed
    // fetch tabDatas again incase something changed
    // clone in case anyone using pure components
    tabDatas = this.state.tabDatas.slice()
    tabDatas[tabIndex].flights = flights
    this.setState({
      tabDatas, loadingFlights: false
    })
  }

  async fetchFlight(tabIndex, flightIndex) {
    const flight = this.state.tabDatas[tabIndex].flights.result[flightIndex]
    var tabDatas = this.state.tabDatas.slice()
    tabDatas[tabIndex].flightMap = Object.assign({}, tabDatas[tabIndex].flightMap, {[flightIndex]: {loading: true}})
    this.setState({tabDatas})

    const detail = await this.client.flight({
      source: 'travelport',
      'od1.id': flight.od1.id,
      'od2.id': flight.od2 ? flight.od2.id : null,
      ptc_adt: flight.fares[0].ptc_breakdown_map.adt.quantity,
      ptc_cnn: flight.fares[0].ptc_breakdown_map.cnn ? flight.fares[0].ptc_breakdown_map.cnn.quantity : 0,
      ptc_inf: flight.fares[0].ptc_breakdown_map.inf ? flight.fares[0].ptc_breakdown_map.inf.quantity : 0,
    })

    tabDatas = this.state.tabDatas.slice()
    tabDatas[tabIndex].flightMap = Object.assign({}, tabDatas[tabIndex].flightMap, {[flightIndex]: detail})
    this.setState({tabDatas})
  }

  async selectFare(tabIndex, flightIndex, fareId) {
    var tabDatas = this.state.tabDatas.slice()
    const flight = this.state.tabDatas[tabIndex].flights.result[flightIndex]
    tabDatas[tabIndex].selection = {flight, fareId}

    this.props.cart.addFlight(flight, fareId)

    if (tabDatas[tabIndex].linkedTab != null && tabDatas[tabDatas[tabIndex].linkedTab].selection == null) {
      this.switchTab(tabDatas[tabIndex].linkedTab)
    } else {
      this.props.openCart()
    }
  }

  render() {
    const tabIndex = this.state.tabIndex
    const tabDataMissing = tabIndex == null ? true : this.state.tabDatas[tabIndex] == null
    const flights = tabDataMissing ? {} : this.state.tabDatas[tabIndex].flights
    const flightMap = tabDataMissing ? {} : this.state.tabDatas[tabIndex].flightMap
    const flightsParams = tabDataMissing ? {} : this.state.tabDatas[tabIndex].flightsParams

    return (
      <div style={{minHeight: 1000, backgroundColor: '#fff', margin: '48px', padding: 32}}>
        <Head>
          <title>{config.defaultTitle} | Flights</title>
        </Head>
          <h1>Flights</h1>
          <FlightsParamsAdvancedForm
            client={this.client}
            onTypeChange={this.typeChange}
            onChange={(value) => this.paramsChange(value)}
            onSearch={(value) => this.addNewTabs(value)}
            onSwapClick={() => this.swap()}
            value={this.state.flightsParam}
            type={this.state.type}
            defaultIataCodes={['SIN', 'BKK', 'KUL', 'HKG']}
            loading={this.state.loadingFlights}
          />
          <div style={{marginTop: 20}}>&nbsp;</div>
          <Tabs
            activeKey={this.state.tabIndex.toString()}
            onTabClick={(key) => this.switchTab(key)}>
            {this.state.tabDatas.slice().reverse().map((tabData, index) => {
              const flightsParams = tabData.flightsParams
              return <Tabs.TabPane
                tab={
                  <div>
                    <div>{flightsParams['od1.origin_airport.code']} -> {nullOrEmpty(flightsParams['od1.destination_airport.code']) ? flightsParams['od2.origin_airport.code'] : flightsParams['od1.destination_airport.code']} ({flightsParams['od1.origin_datetime']})</div>
                    {!nullOrEmpty(flightsParams['od2.origin_datetime']) && <div>{flightsParams['od2.origin_airport.code']} -> {nullOrEmpty(flightsParams['od2.destination_airport.code']) ? flightsParams['od1.origin_airport.code'] : flightsParams['od2.destination_airport.code']} ({flightsParams['od2.origin_datetime']})</div>}
                  </div>}
                key={(this.state.tabDatas.length - index - 1).toString()}>
              </Tabs.TabPane>
            })}


          </Tabs>
          {flights.result == null || flights.result.length === 0
            ? <NoResult
                style={{paddingTop: 128}}
                response={flights}
                type="flights"
                loadingMessage="Searching for the lowest fares...">
              </NoResult>
            : <FlightsResult
                cart={this.props.cart}
                flights={flights.result}
                flightMap={flightMap}
                onFlightClick={(index, flight) => this.fetchFlight(tabIndex, index)}
                onFareClick={(index, flight, fareId) => this.selectFare(tabIndex, index, fareId)} />}
      </div>
    )
  }
}
