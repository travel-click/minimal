import NextError from 'next/error';
import Rollbar from 'rollbar';
import config from '../customize/config';

class MyError extends NextError {
  static getInitialProps = async (context) => {
    if (context.err) {
      const rollbar = new Rollbar({
        enabled: context.req.headers.host.substr(0, 9) !== 'localhost',
        accessToken: config.rollbarAccessToken,
        captureUncaught: false,
        captureUnhandledRejections: false,
        payload: {
          tcUser: config.tcUser
        }
      });
      rollbar.error(context.err);
    }
    const errorInitialProps = (NextError as any).getInitialProps(context);
    return errorInitialProps;
  };
}

export default MyError;